//
//  NetworkAdapter.m
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//

// 网络适配器
#import "NetworkAdapter.h"
#import "NetworkEngine.h"

@implementation NetworkAdapter

+(instancetype)sharedAdapter{
    static NetworkAdapter *_sharedAdapter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAdapter = [[NetworkAdapter alloc] init];
    });
    return _sharedAdapter;
}

#pragma mark - 短链接
-(void)fetchWithPath:(NSString *)path requestParams:(NSDictionary *)requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:(FetchCompletionHandler)block{
    __weak typeof(self)weakSelf = self;
    
    NetworkEngine *networkEngine;
    
        NSString *baseURLString = @"";
        if (API_VER.length){
            if (API_PORT.length){
                baseURLString = [NSString stringWithFormat:@"http://%@:%@/%@/",API_HOST,API_PORT,API_VER];
            } else {
                baseURLString = [NSString stringWithFormat:@"http://%@/%@",API_HOST,API_VER];
            }
        } else {
            if (API_PORT.length){
                baseURLString = [NSString stringWithFormat:@"http://%@:%@/%@/",API_HOST,API_PORT,API_VER];
            } else {
                baseURLString = [NSString stringWithFormat:@"http://%@/%@",API_HOST,API_VER];
            }
        }
        
        networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:baseURLString]];
    
    
    [networkEngine fetchWithPath:path requestParams:requestParams responseObjectClass:responseObjectClass succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if ([responseObject isKindOfClass:[NSDictionary class]]){           // 字典类别
                if ([path hasPrefix:@"api"]){
                    FetchModel *responseModelObject = (FetchModel *)[[responseObjectClass alloc] initWithJSONDict:responseObject];
                    block(YES,responseModelObject,nil);
                    return;
                }
                
                // 判断是否请求成功
                NSNumber *errorNumber = [responseObject objectForKey:@"code"];
                
                if (errorNumber.intValue == 0){
                    FetchModel *responseModelObject = (FetchModel *)[[responseObjectClass alloc] initWithJSONDict:[responseObject objectForKey:@"data"]];
                    block(YES,responseModelObject,nil);
                } else {                                        // 业务错误
                    NSString *errorInfo = [responseObject objectForKey:@"msg"];
                    if (!errorInfo.length){
                        errorInfo = @"系统错误，请联系管理员";
                    }
                    NSDictionary *dict = @{NSLocalizedDescriptionKey: errorInfo};
                    NSError *bizError = [NSError errorWithDomain:PDBizErrorDomain code:errorNumber.integerValue userInfo:dict];
                    [[UIAlertView alertViewWithTitle:@"系统出错" message:errorInfo buttonTitles:@[@"确定"] callBlock:NULL]show];
                    block(NO,responseObject,bizError);
                    return;
                }
            }
        } else {
             block(NO,nil,nil);
        }
    }];
}




@end

