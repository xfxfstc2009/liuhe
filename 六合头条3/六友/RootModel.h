//
//  RootModel.h
//  PPWhaleBaojia
//
//  Created by 裴烨烽 on 2018/12/6.
//  Copyright © 2018 PPWhale. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RootAdModel : FetchModel

@property (nonatomic,copy)NSString *href;
@property (nonatomic,copy)NSString *img;
@property (nonatomic,copy)NSString *status;
@property (nonatomic,assign)NSInteger stay;
@property (nonatomic,assign)NSInteger cancel;

@end

@interface UpgradeModel : FetchModel

@property (nonatomic,assign)CGFloat version;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *content;
@property (nonatomic,copy)NSString *cancel;
@property (nonatomic,copy)NSString *sure;
@property (nonatomic,copy)NSString *download;

@end


@interface RootModel : FetchModel

@property (nonatomic,strong)RootAdModel *ad;
@property (nonatomic,strong)UpgradeModel *upgrade;

@property (nonatomic,copy)NSString *link;
@property (nonatomic,copy)NSString *statusbarColor;

@end



NS_ASSUME_NONNULL_END
