//
//  GWImageView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWImageView.h"
#import <UIImageView+WebCache.h>


@implementation GWImageView

-(void)uploadWithNormal:(NSString *)url size:(CGSize)size back:(void(^)(UIImage *img))back{
    if (![url isKindOfClass:[NSString class]]){
        return;
    }
    // 加载图片
    NSURL *imgURL = [NSURL URLWithString:url];
    if ([url hasPrefix:@"http://"]){
        imgURL = [NSURL URLWithString:url];
    } else {

    }
    
    UIImage *placeholder = [UIImage imageNamed:@"giganticWhale"];
    
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (!image){
            image = placeholder;
        }
        if (back){
            back(image);
        }
    }];
}

// 更新图片
-(void)uploadImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    // 加载图片
    NSURL *imgURL = [NSURL URLWithString:urlString];
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"placeholder"];
    }
    if ([urlString hasPrefix:@"http://"]){
        imgURL = [NSURL URLWithString:urlString];
    } else {

    }
    
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.image = image;
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

-(void)uploadImgWithNormal:(NSString *)url placehorder:(UIImage *)placeholder back:(void(^)(UIImage *img))back{
    [self uploadImgWithNormal:url placeholder:placeholder imgType:ImgTypeNormal callback:^(UIImage *image) {
        if (back){
            back(image);
        }
    }];
}

-(void)uploadImgWithOriginal:(NSString *)url placehorder:(UIImage *)placeholder back:(void (^)(UIImage *))back{
    [self uploadImgWithNormal:url placeholder:placeholder imgType:ImgTypeOriginal callback:^(UIImage *image) {
        if (back){
            back(image);
        }
    }];
}

#pragma mark - 1. 更新其他图片
-(void)uploadImgWithNormal:(NSString *)urlString placeholder:(UIImage *)placeholder imgType:(ImgType)type callback:(void(^)(UIImage *image))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    NSURL *imgURL;
    if ([urlString hasPrefix:@"http://"] || [urlString hasPrefix:@"https://"]){
        imgURL = [NSURL URLWithString:urlString];
    } else if ([urlString hasPrefix:@"smartmin.img-cn-shanghai.aliyuncs.com"]){

    } else {

    }
    if (type == ImgTypeOriginal){
//        imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",aliyunOSS_BaseURL,urlString]];
    }
    
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image != nil){
            self.image = image;
        } else {
            self.image = placeholder;
        }
        
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}



@end
