//
//  AccountModel.h
//  PPWhaleBaojia
//
//  Created by 裴烨烽 on 2017/9/7.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"
#import "RootModel.h"

@interface AccountModel : FetchModel


+(instancetype)sharedAccountModel;                                                               /**< 单例*/
@property (nonatomic,strong)RootModel *rootModel;
@property (nonatomic,copy)NSString *webUrl;
@property (nonatomic,copy)NSString *share_title;
@property (nonatomic,copy)NSString *share_content;
@property (nonatomic,copy)NSString *share_img;
@property (nonatomic,copy)NSString *share_url;
@property (nonatomic,copy)NSString *lungch_img;
@property (nonatomic,copy)NSString *lungch_content;
@property (nonatomic,assign)BOOL isShenhe;

@property (nonatomic,copy)NSString *link;
@property (nonatomic,assign)NSInteger appIndex;


@property (nonatomic,assign)CGFloat tabBarHeight;
@property (nonatomic,assign)CGFloat navBarHeight;
@property (nonatomic,assign)CGFloat statusHeight;
@property (nonatomic,assign)CGFloat bottomHeight;


@property (nonatomic,assign)BOOL webHasLoaded;
@property (nonatomic,assign)BOOL adHasLoaded;
@property (nonatomic,assign)BOOL hasError;

@end

