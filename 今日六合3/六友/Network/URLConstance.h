//
//  URLConstance.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#ifndef URLConstance_h
#define URLConstance_h

// 【接口环境】
#ifdef DEBUG        // 测试           // 【JAVA】
#define API_HOST @""//192.168.0.163
#define API_PORT @""
#else               // 线上
#define API_HOST  @""
#define API_PORT  @""
#endif

static NSString *const setting = @"v1/app/ios1";


#endif /* URLConstance_h */
