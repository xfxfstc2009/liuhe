//
//  WebViewController.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/27.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWWebView.h"
#import <SDWebImage/UIImageView+WebCache.h>                     // 网络图片加载

@interface WebViewController : UIViewController

+(instancetype)sharedAccountModel;

@property (nonatomic,strong)GWWebView *mainWebView;
@property (nonatomic,strong)UIImageView *imgView;
@property (nonatomic,strong)UIView *statusBarView;
@property (nonatomic, strong) UIActivityIndicatorView * activityIndicator;

-(void)webFinash;
-(void)loadWeb;
-(void)loadUmen;
-(void)showUpload;

@end

