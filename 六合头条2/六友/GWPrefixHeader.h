//
//  GWPrefixHeader.h
//  PPWhaleBaojia
//
//  Created by 裴烨烽 on 2017/7/14.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#ifndef GWPrefixHeader_h
#define GWPrefixHeader_h

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <objc/runtime.h>
#import "GWImageView.h"
#import "UIColor+Extended.h"

#import "GWTool.h"
#import "NSString+LCCalcSize.h"
#import "UIView+LCGeometry.h"
#import "UIButton+Customise.h"
#import "UIAlertView+Customise.h"
#import "AccountModel.h"
#import "UIView+MaterialDesign.h"
#import "WebViewController.h"
#import <UMAnalytics/MobClick.h>

// System Default Sets
#define USERDEFAULTS     [NSUserDefaults standardUserDefaults]
#define NOTIFICENTER     [NSNotificationCenter defaultCenter]


// System
#define IS_IOS7_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 6.99)
#define IS_IOS8_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 7.99)
#define IS_IOS9_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 8.99)
#define IS_IOS10_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 9.99)

// 【设备】
#define ipadMini2  ([UIScreen mainScreen].bounds.size.height == 1024)
#define  iPhone4_4s     ([UIScreen mainScreen].bounds.size.width == 320.f && [UIScreen mainScreen].bounds.size.height == 480.f)
#define  iPhone5_5s     ([UIScreen mainScreen].bounds.size.width == 320.f && [UIScreen mainScreen].bounds.size.height == 568.f)
#define  iPhone6_6s     ([UIScreen mainScreen].bounds.size.width == 375.f && [UIScreen mainScreen].bounds.size.height == 667.f)
#define  iPhone6_6sPlus ([UIScreen mainScreen].bounds.size.width == 414.f && [UIScreen mainScreen].bounds.size.height == 736.f)
#define IS_iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone5  ([UIScreen mainScreen].bounds.size.height == 568)
// 【Frame】
#define kScreenBounds               [[UIScreen mainScreen] bounds]
#define kScreenWidth                kScreenBounds.size.width
#define kScreenHeight               kScreenBounds.size.height

// System Style
#define RGB(r, g, b,a)    [UIColor colorWithRed:(r)/255. green:(g)/255. blue:(b)/255. alpha:a]
#define BACKGROUND_VIEW_COLOR       RGB(48,121,255,1)
#define NAVBAR_COLOR RGB(255, 255, 255,1)
#define Main_Color [UIColor colorWithRed:71/255.0 green:165/255.0 blue:254/255.0 alpha:1]



#define linkUrl @"https://app.liuhetoutiaostatic.com/v1/app/toutiao-ios2"
#define youmeng @"5c358f22b465f54a19000b4e"


#endif /* GWPrefixHeader_h */
