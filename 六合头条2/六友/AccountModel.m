//
//  AccountModel.m
//  PPWhaleBaojia
//
//  Created by 裴烨烽 on 2017/9/7.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "AccountModel.h"

@implementation AccountModel

+(instancetype)sharedAccountModel{
    static AccountModel *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[AccountModel alloc] init];
    });
    return _sharedAccountModel;
}


-(NSInteger)appIndex{
    return 1;
}



-(CGFloat)tabBarHeight{
    if (IS_iPhoneX){
        return  49 + 34;
    } else {
        return  49 ;
    }
}
-(CGFloat)navBarHeight{
    if (IS_iPhoneX){
        return  44 + 44;
    } else {
        return  20 + 44;
    }
}

-(CGFloat)statusHeight{
    if (IS_iPhoneX){
        return  44 ;
    } else {
        return  20 ;
    }
}


-(CGFloat)bottomHeight{
    if (IS_iPhoneX){
        return 44 + 34 - 15;
    } else {
        return 44;
    }
}


@end
