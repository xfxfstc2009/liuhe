//
//  GWImageView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,ImgType) {
    ImgTypeNormal,
    ImgTypePlayer,
    ImgTypeLaungch,
    ImgTypeOriginal,                /**< 原图*/
};

@interface GWImageView : UIImageView

-(void)uploadImgWithNormal:(NSString *)url placehorder:(UIImage *)placeholder back:(void(^)(UIImage *img))back;
-(void)uploadWithNormal:(NSString *)url size:(CGSize)size back:(void(^)(UIImage *img))back;
// 3. 获取原图
-(void)uploadImgWithOriginal:(NSString *)url placehorder:(UIImage *)placeholder back:(void (^)(UIImage *))back;
@end

